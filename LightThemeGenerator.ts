/** @format */

// noprotect

import chroma from "chroma-js"
import { parse } from "papaparse"

type ReplacementConfig = {
  originalColor: string
  replaceWith: Promise<string>
  atIndices: number[]
}

type Spec = {
  originalColor: string
  desiredContrast: number
  newColor: string
}

export default class LightThemeGenerator {
  private readonly specList: Spec[] = []
  private readonly replacementPlan: ReplacementConfig[] = []
  private readonly paletteDictionary: { [key: string]: string } = {}

  constructor(
    private readonly csvSpec: string,
    private readonly filename: string,
    private readonly themeString: string,
    private readonly oldBgColor: string, // dark theme's background color
    private readonly newBgColor: string, // light theme's background color
  ) {
    this.specList = parse<[string, string, string]>(this.csvSpec, {
      comments: "//",
      skipEmptyLines: true,
    }).data.map((r) => ({
      originalColor: r[0],
      desiredContrast: parseFloat(r[1]),
      newColor: r[2],
    }))
    this.specList.shift()

    LightThemeGenerator.validate(this.specList, themeString)
    this.main()
  }

  private async main(): Promise<void> {
    for (let spec of this.specList) {
      const promisedColor = this.computeNewColor(spec)
      if (!promisedColor) continue

      this.replacementPlan.push(this.getReplacementConfig(promisedColor, spec))
    }

    const themeStringArray = this.themeString.split("")
    this.replacementPlan.forEach((c) => {
      c.atIndices.forEach((i) => {
        c.replaceWith.then((replacement) => {
          themeStringArray[i] = replacement
          this.paletteDictionary[c.originalColor] = replacement
        })

        for (let j = 0; j < c.originalColor.length - 1; j++) {
          themeStringArray[j + i + 1] = ""
        }
      })
    })

    // Make sure all promises are resolved
    await Promise.all(this.replacementPlan.map((c) => c.replaceWith))

    console.log(this.paletteDictionary)
    console.log("DONE")

    LightThemeGenerator.downloadFile(themeStringArray.join(""), this.filename)
  }

  private getReplacementConfig(
    promisedColor: Promise<string>,
    s: Spec,
  ): ReplacementConfig {
    const replacementConfig: ReplacementConfig = {
      originalColor: s.originalColor,
      replaceWith: promisedColor,
      atIndices: [],
    }

    const re = new RegExp(s.originalColor, "gi")
    for (let m of this.themeString.matchAll(re)) {
      if (m.index) replacementConfig.atIndices.push(m.index)
    }

    return replacementConfig
  }

  private computeNewColor(s: Spec): Promise<string> | null {
    let promisedColor: Promise<string> | null = null

    if (s.newColor) {
      if (s.newColor !== s.originalColor) return Promise.resolve(s.newColor)
      return null
    }

    if (!s.desiredContrast && !s.newColor) {
      // get newColor by tweaking the lightness until reaching the same contrast as in the original theme
      s.desiredContrast = chroma.contrast(this.oldBgColor, s.originalColor)
    }

    // get newColor by tweaking lightness until desiredContrast reached
    promisedColor = this.changeLightForContrast(
      s.originalColor,
      s.desiredContrast,
    )

    return promisedColor
  }

  private changeLightForContrast(
    color: string,
    contrast: number,
  ): Promise<string> | null {
    const currentContrast = chroma.contrast(color, this.newBgColor)

    if (currentContrast < contrast) {
      return this.darken(color, contrast)
    } else if (currentContrast > contrast) {
      return this.brighten(color, contrast)
    } else {
      return null
    }
  }

  private static validate(csvData: Spec[], themeString: string): void {
    if (!csvData || !csvData[0]) throw new Error("No data found in the CSV!")
    if (!themeString) throw new Error("Empty Theme file!")

    csvData.forEach((spec, idx) => {
      let { originalColor } = spec
      if (!originalColor) {
        throw new Error(
          "Could not find the original color (first column) on line " +
            (idx + 2),
        )
      }
    })
  }

  // Download the new theme file when finished
  private static downloadFile(str: string, filename: string) {
    const blob = new Blob([str], { type: "text/plain" })

    const el = document.createElement("a")
    el.setAttribute("href", window.URL.createObjectURL(blob))
    el.setAttribute("download", "light_" + filename)
    document.body.appendChild(el)
    el.click()
    document.body.removeChild(el)
  }

  // Incrementally darken the suppliedColor until desired contrast with the baseColor is reached
  private async darken(
    color: string,
    desiredContrast: number,
  ): Promise<string> {
    return new Promise((resolve) => {
      let chromaColor = chroma(color)
      const chromaBgColor = chroma(this.newBgColor)
      while (chroma.contrast(chromaColor, chromaBgColor) < desiredContrast) {
        chromaColor = chroma(chromaColor).darken(0.001)
      }

      resolve(chromaColor.hex())
    })
  }

  // Incrementally brighten the suppliedColor until desired contrast with the baseColor is reached
  private async brighten(
    color: string,
    desiredContrast: number,
  ): Promise<string> {
    return new Promise((resolve) => {
      let chromaColor = chroma(color)
      const chromaBgColor = chroma(this.newBgColor)
      while (chroma.contrast(chromaColor, chromaBgColor) > desiredContrast) {
        chromaColor = chroma(chromaColor).brighten(0.001)
      }

      resolve(chromaColor.hex())
    })
  }
}
