/** @format */

// noprotect

import Main from "./LightThemeGenerator"

const csvInput = document.getElementById("csv") as HTMLInputElement
const themeInput = document.getElementById("dark_theme") as HTMLInputElement
const bgColorInput = document.getElementById("bg_color") as HTMLInputElement
const fgColorInput = document.getElementById("fg_color") as HTMLInputElement
const button = document.getElementById("button_input") as HTMLButtonElement

button.onclick = async () => {
  new Main(
    await csvInput.files![0].text(),
    themeInput.files![0].name,
    await themeInput.files![0].text(),
    bgColorInput.value,
    fgColorInput.value,
  )
}
