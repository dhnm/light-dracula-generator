<!-- @format -->

# Light Dracula Generator

Parses provided text file and replaces colors according to the specification provided in a csv file.

## Instructions

```
npm install
npm run dev
```

Open browser and follow the instructions. You will need 4 inputs:

1. CSV file

- Configuration file. You should be able to use the provided ones with some success.
- First column is compulsory and describes the Dracula's colors to be replaced.
- OPTIONAL: The second column is the desired contrast of the new generated color relative to the new background.
- OPTIONAL: The third column is direct replacement color. It is useful for swapping the background and foreground color.
- If both 2nd and 3rd columns are omitted, the program will generate the new color based on the original contrast between the provided color and the dark background color.

2. Theme file

- The text file containing the strings to be replaced. `yml` for VSCode, `tmTheme` for Sublime Text, `css` for Panic, etc.

3. Dark theme's background color

- Should be #282a36 for Dracula

4. New theme's background color

- #f8f8f2 for color swap -> "sunset", or #ffffff for white

## Acknowledgements

I followed the "Light Theme Methodology" as described here: [https://github.com/ashrafhadden/dracula.min#light-theme-methodology]

I used [Chroma.js][https://github.com/gka/chroma.js] for color manipulation, and [Papa Parse][https://www.papaparse.com] for CSV parsing.

I made these themes with this tool:

- [Dracula Light for Panic Nova][https://gitlab.com/dhnm/dracula-light-for-nova] (Sunset and White)
- [Dracula Light for Sublime Text 3][https://gitlab.com/dhnm/dracula-light-for-sublime-text] (Sunset and White)

MIT License
